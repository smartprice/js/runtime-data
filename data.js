const PATH = require('path');
const FS = require('fs');
const READERS = require('./impl/readers.js');
const HashMap = require('./impl/hashmap.js');
const Heap = require('./impl/heap.js');

const TTL = 1000 * 60;
var CACHE = {};

// those should be in sync across all services!
const MAX_MEMORY_LEN = 10;
const MAX_COLOR_LEN = 30;
const PRODUCT_CONDITION_IDS = {
    'like_new': 1,
    'best': 2,
    'works': 3,
    'n_grade_best': 4,
    'n_grade_works': 5,
    'critical': 6,
    'new': 7,
};

module.exports = function(version) {
    let baseDir = PATH.join(process.env.RUNTIME_DATA_DIR, 'dicts');

    if (version === undefined) {
        const lastVersionFile = PATH.join(baseDir, 'LAST', 'VERSION');
        version = parseInt(FS.readFileSync(lastVersionFile).toString().trim(), 10);

    } else {
        version = parseInt(version, 10);
    }

    version = '' + version;
    baseDir = PATH.join(baseDir, version);

    let _getDict = function() {
        return getDict.apply(undefined, [baseDir, version].concat(Array.from(arguments)));
    };

    let clibContext = {};
    let getClib = function() {
        const path = PATH.join.apply(undefined, [baseDir].concat(arguments[1]));

        if (path in clibContext) {
            return clibContext[path];
        }

        clibContext[path] = arguments[0].construct.apply(undefined, [path].concat(Array.from(arguments).slice(2)));

        return clibContext[path];
    };

    this.delete = function() {
        for (let node of Object.values(clibContext)) {
            node.delete();
        }

        this.delete = function() {};
    };

    this.getProducts = function(spec) {
        const data = _getDict(['products']);

        if (spec.id !== undefined) {
            return data['by_id'][spec.id];
        }

        if (spec.type !== undefined) {
            let node = data['tree'][spec.type] || {};

            if (spec.vendor === undefined) {
                let out = [];

                for (let vendorData of node) {
                    for (let model of vendorData) {
                        out.push(model);
                    }
                }

                return out;

            } else {
                node = node[spec.vendor] || {};

                if (spec.model === undefined) {
                    return Object.values(node);

                } else {
                    return node[spec.model];
                }
            }
        }

        throw 'Either use "id" or "type[->vendor[->model]]" lookup';
    };

    this.getKnownVendors = function(type) {
        return Object.keys(_getDict(['products'])['tree'][type] || {});
    };

    this.getProductColors = function(id) {
        const product = this.getProducts({'id': id});

        return _getDict(['detector_names'], product['vendor'])['colors'][product['model']]['all'];
    };

    this.getSiteStats = function() {
        const data = _getDict(['site_stats'])

        return data
    }

    this.getProductVariants = function(spec) {
        const main = getClib(HashMap, ['productvariants', 'main']);

        if (spec.id !== undefined) {
            return hashmapGetByKey(parseInt(spec.id, 10), main);
        }

        if (spec.product !== undefined) {
            let parts = [dumpUI64(spec.product)];
            let prefixLength = parts[0].length;

            if (spec.memory !== undefined) {
                parts.push(dumpFixed(spec.memory, MAX_MEMORY_LEN, ' '));
                prefixLength += MAX_MEMORY_LEN;

                if (spec.color !== undefined) {
                    parts.push(dumpFixed(spec.color, MAX_COLOR_LEN, ' '));
                    prefixLength += MAX_COLOR_LEN;
                }
            }

            const prefix = Buffer.concat(parts, prefixLength);
            const tree = getClib(Heap, ['productvariants', 'tree']);

            if (parts.length == 3) {
                const node = tree.get(prefix);

                if (node === undefined) {
                    return undefined;
                }

                return hashmapGetByKey(READERS.UI64.read(node.subarray(node.length - 8, node.length)), main);

            } else {
                let out = [];
                let it = tree.getAll(prefix);

                for (let i; (i = it.next()) !== undefined;) {
                    const node = hashmapGetByKey(READERS.UI64.read(i.subarray(i.length - 8, i.length)), main);

                    if (node !== undefined) {
                        out.push(node);
                    }
                }

                it.delete();

                return out;
            }
        }

        throw 'Either use "id" or "product[->memory[->color]]" lookup';
    };

    this.productVariantsLookup = function(spec) {
        let productSpec = {
            'type': spec.type
        };

        for (let key of [
            'vendor',
            'model'
        ]) {
            if (spec[key] !== undefined) {
                productSpec[key] = spec[key];
            }
        }

        let variantSpec = {};

        for (let key of [
            'memory',
            'color'
        ]) {
            if (spec[key] !== undefined) {
                variantSpec[key] = spec[key];
            }
        }

        let products = this.getProducts(productSpec);

        if (!Array.isArray(products)) {
            if (products === undefined) {
                return [];
            }

            products = [products];
        }

        let out = [];

        for (let product of products) {
            variantSpec['product'] = product.id;

            const variants = this.getProductVariants(variantSpec);

            if (Array.isArray(variants)) {
                out = out.concat(variants);

            } else if (variants !== undefined) {
                out.push(variants);
            }
        }

        return out;
    };

    this.getVisibleProducts = function() {
        return _getDict(['visible'])['products'];
    };

    this.getVisibleVariants = function() {
        return _getDict(['visible'])['variants'];
    };

    this.getVisibleMultipliers = function() {
        return _getDict(['visible'])['multipliers'];
    };

    this.getCatalogMultiplierIds = function() {
        return _getDict(['visible'])['catalog'];
    };

    this.getSaleMultiplierIds = function() {
        return _getDict(['visible'])['sale'];
    };

    this.getDiscountMultiplierIds = function() {
        return _getDict(['visible'])['discount'];
    };

    this.getNormalizationMaps = function() {
        return _getDict(['site_normalizations']);
    };

    this.getPriceMultiplier = function(spec) {
        if (spec.id === undefined) {
            const key = Buffer.concat([
                dumpUI64(spec.variant),
                dumpUI64(PRODUCT_CONDITION_IDS[spec.condition])
            ]);

            const data = getClib(HashMap, ['price_multipliers', 'variant_condition'], String);

            spec.id = hashmapGetByKey(key, data, READERS.UI64);

            if (spec.id === undefined) {
                return undefined;
            }

        } else {
            spec.id = parseInt(spec.id, 10);
        }

        return hashmapGetByKey(spec.id, getClib(HashMap, ['price_multipliers', 'main']));
    };

    this.getCRMProductDiscountReason = function(spec) {
        const data = _getDict(['crm_product_discount_reasons']);

        if (spec.promo !== undefined) {
            spec.id = data['by_promo'][spec.promo.toLowerCase()];

            if (spec.id === undefined) {
                return undefined;
            }
        }

        return data['by_id'][spec.id];
    };

    this.getCRMProductRejectionReason = function(id) {
        return _getDict(['crm_product_rejection_reasons'])['by_id'][id];
    };

    this.getCRMProductRejectionReasonIds = function() {
        return Object.keys(_getDict(['crm_product_rejection_reasons'])['by_id']).map(function(id) {
            return parseInt(id, 10);
        });
    };

    this.getPartnerProductTypes = function(id) {
        const data = _getDict(['partners_product_types'])['by_id'];

        if (id === undefined) {
            return data;
        }

        return data[id];
    };

    this.ldapPartnermap = function(key) {
        return hashmapGetByKey(key, getClib(HashMap, ['ldap_partnermap', 'main'], String));
    };

    this.getPointsPartnerIndex = function() {
        return _getDict(['points_indexes'])['partner_index'];
    };

    this.ldapPointmap = function(key) {
        return hashmapGetByKey(key, getClib(HashMap, ['ldap_pointmap', 'main'], String));
    };

    this.ldapUsermap = function(key) {
        const data = getClib(HashMap, ['ldap_pointmap', 'main'], String);

        if (key === undefined) {
            return data;
        }

        return hashmapGetByKey(key, data);
    };

    this.ldapUsermapExtTradein = function(key) {
        return hashmapGetByKey(key, getClib(HashMap, ['ldap_usermap_ext_tradein', 'main'], String));
    };

    this.ldapUsermapExtOTLogistics = function(key) {
        return hashmapGetByKey(key, getClib(HashMap, ['ldap_usermap_ext_ot_logistics', 'main'], String));
    };

    this.getLegalEntities = function(id) {
        return _getDict('legal_entities')[id];
    };

    this.getDeliveryData = function(id) {
        return hashmapGetByKey(parseInt(id, 10), getClib(HashMap, ['delivery', 'main']));
    };

    this.getCities = function(id) {
        return hashmapGetByKey(parseInt(id, 10), getClib(HashMap, ['cities', 'main']));
    };

    this.getCDEKCities = function(id) {
        id = hashmapGetByKey(parseInt(id, 10), getClib(HashMap, ['cities', 'cdek_main']), READERS.UI64);

        if (id === undefined) {
            return undefined;
        }

        let out = Object.assign({}, this.getCities(id));
        out['ID'] = out['cdek_code'];
        delete out['cdek_code'];

        return out;
    };

    this.getCDEKPVZ = function(pvzCode) {
        return hashmapGetByKey(pvzCode, getClib(HashMap, ['cdek_points'], String));
    };

    this.getBoxberryPVZ = function(pvzCode) {
        return hashmapGetByKey(pvzCode, getClib(HashMap, ['boxberry_points'], String));
    };

    this.getBoxberryCities = function(id) {
        return hashmapGetByKey(id, getClib(HashMap, ['boxberry_cities'], String));
    };

    this.getDalliCities = function(id) {
        return hashmapGetByKey(id, getClib(HashMap, ['dalli_cities'], String));
    };

    this.getCRMStates = function(spec) {
        const data = _getDict(['crmstates']);

        if (spec.id !== undefined) {
            return data['by_id'][spec.id];
        }

        if (spec.flag !== undefined) {
            return data['flag2ids'][spec.flag] || [];
        }

        if (spec.type !== undefined) {
            return data['type2ids'][spec.type] || [];
        }

        if (spec.readRole !== undefined) {
            return data['read_role2ids'][spec.readRole] || [];
        }

        if (spec.writeRole !== undefined) {
            return data['write_role2ids'][spec.writeRole] || [];
        }

        throw 'Invalid arguments';
    };

    this.getCRMStateParameterValues = function(spec) {
        const data = _getDict(['crmstateparametervalues']);

        return (data[spec.reverse ? 'reverse_tree' : 'tree'][spec.state] || {})[spec.parameter] || [];
    };

    this.getKnownPhones = function(multiplierMethod) {
        return _getDict(['known_phones', multiplierMethod]);
    };

    this.getTop24 = function() {
        return _getDict(['top24']).legacy;
    };

    this.getCatalogMemoryList = function() {
        return _getDict(['catalog_data', 'memory_list']).data;
    };

    this.getProductCharacteristics = function(spec) {
        spec = normalizedVendorAndModel(this, spec);

        return _getDict(['static_product_data', 'characteristics', spec.vendor, spec.model]);
    };

    this.getProductFeaturesFilePath = function(spec) {
        spec = normalizedVendorAndModel(this, spec);

        const path = PATH.join(baseDir, 'static_product_data', 'features', spec.vendor, spec.model + '.html');

        if (FS.existsSync(path)) {
            return path;
        }

        return undefined;
    };

    this.getSiteBanners = function(type) {
        return _getDict(['site_banners', 'data'])[type] || [];
    };

    this.getProductPhotosList = function(spec) {
        spec = Object.assign(spec, normalizedVendorAndModel(this, spec));

        if (!('type' in spec)) {
            spec.type = 'default';
        }

        if (spec.color !== undefined) {
            const colorPhoto = (_getDict(['static_product_data', 'color_photo', spec.vendor, spec.model]) || {})[spec.color] || [];
            let colorPhotoType = [];

            for (let photo of colorPhoto) {
                if (spec.type == 'default') {
                    if (photo['file'].indexOf('-') == -1) {
                        colorPhotoType.push(photo);
                    }

                } else {
                    if (photo['file'].indexOf(spec.type) > -1) {
                        colorPhotoType.push(photo);
                    }
                }
            }

            if (colorPhotoType.length > 0) {
                return colorPhotoType;
            }
        }

        return (_getDict(['static_product_data', 'photo', spec.vendor, spec.model]) || {})[spec.type] || [];
    };

    this.getMenuProducts = function() {
        return _getDict(['menu_products'])['menu'];
    };

    this.getPriceAffectingProductParameterIds = function() {
        return _getDict(['productparameterbasepricemultipliers'])['effective_parameters'];
    };

    this.getBaseProductParameterPriceMultipliers = function() {
        return Object.values(_getDict(['productparameterbasepricemultipliers'])['by_id']);
    };

    this.getBaseProductParameterPriceMultiplier = function(spec) {
        const data = _getDict(['productparameterbasepricemultipliers']);

        if (spec.productParameterIds !== undefined) {
            let pappi = {};

            for (let id of this.getPriceAffectingProductParameterIds()) {
                pappi[id] = true;
            }

            let productParameterIds = [];

            for (let id of spec.productParameterIds) {
                if (id in pappi) {
                    productParameterIds.push(parseInt(id, 10));
                }
            }

            productParameterIds.sort();

            spec.id = data['by_key'][productParameterIds.join('\0')];

            if (spec.id === undefined) {
                return undefined;
            }
        }

        return data['by_id'][spec.id];
    };

    this.getProductParameterPriceMultipliers = function(spec) {
        if (spec.multiplier !== undefined) {
            spec.multiplier = parseInt(spec.multiplier, 10);

            const multiplierBase = getClib(HashMap, ['productparameterpricemultipliers', 'multiplier_base'], String);

            if (spec.base === undefined) {
                const main = getClib(HashMap, ['productparameterpricemultipliers', 'main']);
                let out = [];

                for (let base of this.getBaseProductParameterPriceMultipliers()) {
                    const key = Buffer.concat([
                        dumpUI64(spec.multiplier),
                        dumpUI64(base['id'])
                    ]);

                    const pppmId = hashmapGetByKey(key, multiplierBase, READERS.UI64);

                    if (pppmId === undefined) {
                        continue;
                    }

                    const pppm = hashmapGetByKey(pppmId, main);

                    if (pppm === undefined) {
                        continue;
                    }

                    out.push(pppm);
                }

                return out;

            } else {
                const key = Buffer.concat([
                    dumpUI64(spec.multiplier),
                    dumpUI64(spec.base)
                ]);

                spec.id = hashmapGetByKey(key, multiplierBase, READERS.UI64);

                if (spec.id === undefined) {
                    return undefined;
                }
            }
        }

        return hashmapGetByKey(parseInt(spec.id, 10), getClib(HashMap, ['productparameterpricemultipliers', 'main']));
    };

    this.getProductParameters = function(id) {
        const data = _getDict(['productparameters']);

        if (id === undefined) {
            return Object.values(data['by_id']);
        }

        return data['by_id'][id];
    };
};

function getDict(baseDir, version, nameParts, ext) {
    if (ext === undefined) {
        ext = '.json';
    }

    const name = PATH.join.apply(undefined, nameParts) + ext;
    const now = new Date().getTime();

    if (name in CACHE) {
        if ((now - CACHE[name]['time']) < TTL) {
            if (version in CACHE[name]['data']) {
                if ((now - CACHE[name]['data'][version]['time']) < TTL) {
                    return CACHE[name]['data'][version]['data'];

                } else {
                    delete CACHE[name]['data'][version];
                }
            }

        } else {
            let newData = {};
            let minTime = undefined;

            for (let version_ in CACHE[name]['data']) {
                let data = CACHE[name]['data'][version_];

                if ((now - data['time']) >= TTL) {
                    continue;
                }

                if ((minTime === undefined) || (minTime > data['time'])) {
                    minTime = data['time'];
                }

                newData[version_] = data;
            }

            if (minTime === undefined) {
                delete CACHE[name];

            } else {
                CACHE[name] = {
                    'time': minTime,
                    'data': newData
                };
            }
        }
    }

    const path = PATH.join(baseDir, name);
    let data = undefined;

    try {
        data = JSON.parse(FS.readFileSync(path));

    } catch (e) {
        return undefined;
    }

    if (!(name in CACHE)) {
        CACHE[name] = {
            'time': now,
            'data': {}
        };
    }

    CACHE[name]['data'][version] = {
        'time': now,
        'data': data
    };

    return data;
}

function hashmapGetByKey(key, data, reader) {
    if (reader === undefined) {
        reader = READERS.JSON;
    }

    const out = data.get(key);

    if (out === undefined) {
        return out;
    }

    return reader.read(out);
}

function dumpUI64(value) {
    let buf = Buffer.allocUnsafe(8);
    buf.writeBigUInt64BE(BigInt(parseInt(value, 10)));
    return buf;
}

function dumpFixed(value, size, padding) {
    let buf = Buffer.allocUnsafe(size);
    buf.write(value.padEnd(size, padding));
    return buf;
}

function normalizedVendorAndModel(obj, spec) {
    if (spec.id !== undefined) {
        const product = obj.getProducts({'id': spec.id});

        spec.vendor = product['vendor'];
        spec.model = product['model'];
    }

    const map = obj.getNormalizationMaps()['normalization'];

    return {
        'vendor': map['vendor'][spec.vendor],
        'model': map['model'][spec.model]
    };
}
