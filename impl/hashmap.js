var LIB = require('./decl.js');
var ref = require('ref');

var stringPtr = ref.refType('char');

function HashMapIterator(ptr, keyType) {
    const impl = new ((keyType === Number) ? HashMapIteratorImplInt : HashMapIteratorImplBytes)(ptr);

    this.delete = function() {
        LIB.HashMapIteratorDelete(ptr);

        this.delete = function() {};
    };

    this.next = function() {
        const rv = impl.next();

        if (rv === undefined) {
            this.delete();
        }

        return rv;
    };
}

const HashMap = function(path, keyType) {
    if (keyType === undefined) {
        keyType = Number;

    } else if (keyType !== String) {
        throw 'Supported key types are: Number (default), String';
    }

    const ptr = LIB.HashMapOpen(path);

    this.delete = function() {
        LIB.HashMapDelete(ptr);

        this.delete = function() {};
    };

    this.get = function(key) {
        let size = 0;
        let buf = ref.alloc(stringPtr);

        if ((typeof key).toLowerCase() == 'number') {
            size = LIB.HashMapGetInt(ptr, key, buf);

        } else {
            size = LIB.HashMapGet(ptr, key.length, key, buf);
        }

        if (size == 0) {
            return undefined;
        }

        return ref.reinterpret(buf.deref(), size, 0);
    };

    this.items = function() {
        return new HashMapIterator(LIB.HashMapGetAll(ptr), keyType);
    }
};

HashMap.construct = function(path, keyType) {
    return new HashMap(path, keyType);
};

module.exports = HashMap;

function HashMapIteratorImplBytes(ptr) {
    let keySize = ref.alloc('uint64');
    let key = ref.alloc(stringPtr);
    let valueSize = ref.alloc('uint64');
    let value = ref.alloc(stringPtr);

    this.next = function() {
        if (!LIB.HashMapIteratorNext(ptr, keySize, key, valueSize, value)) {
            return undefined;
        }

        return [
            ref.reinterpret(key.deref(), keySize.deref(), 0),
            ref.reinterpret(value.deref(), valueSize.deref(), 0)
        ];
    };
}

function HashMapIteratorImplInt(ptr) {
    let key = ref.alloc('uint64');
    let valueSize = ref.alloc('uint64');
    let value = ref.alloc(stringPtr);

    this.next = function() {
        if (!LIB.HashMapIteratorNextInt(ptr, key, valueSize, value)) {
            return undefined;
        }

        return [
            key.deref(),
            ref.reinterpret(value.deref(), valueSize.deref(), 0)
        ];
    };
}
