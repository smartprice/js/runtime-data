var LIB = require('./decl.js');
var ref = require('ref');

var stringPtr = ref.refType('char');

function HeapIterator(ptr, recordSize) {
    let buf = ref.alloc(stringPtr);

    this.delete = function() {
        LIB.HeapIteratorDelete(ptr);

        this.delete = function() {};
    };

    this.next = function() {
        const size = LIB.HeapIteratorNext(ptr, buf);

        if (size == 0) {
            this.delete();
            return undefined;
        }

        return ref.reinterpret(buf.deref(), recordSize, 0);
    };
}

const Heap = function(path) {
    const ptr = LIB.HeapOpen(path);
    const recordSize = LIB.HeapRecordSize(ptr);

    this.delete = function() {
        LIB.HeapDelete(ptr);

        this.delete = function() {};
    };

    this.get = function(prefix) {
        let it = this.getAll(prefix);

        for (let i; (i = it.next()) !== undefined;) {
            it.delete();
            return i;
        }

        it.delete();

        return undefined;
    };

    this.getAll = function(prefix) {
        return new HeapIterator(LIB.HeapGetAll(ptr, prefix.length, prefix), recordSize);
    };
};

Heap.construct = function(path) {
    return new Heap(path);
};

module.exports = Heap;
