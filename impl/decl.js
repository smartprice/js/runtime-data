var ffi = require('ffi');
var ref = require('ref');

var stringPtr = ref.refType(ref.refType('char'));
var ui64Ptr = ref.refType('uint64');

module.exports = ffi.Library('/opt/libruntime_data.so', {
    'HeapOpen': ['uint64', ['string']],
    'HeapDelete': ['void', ['uint64']],
    'HeapRecordSize': ['uint64', ['uint64']],
    'HeapGetAll': ['uint64', ['uint64', 'uint64', 'string']],
    'HeapIteratorDelete': ['void', ['uint64']],
    'HeapIteratorNext': ['uint64', ['uint64', stringPtr]],

    'HashMapOpen': ['uint64', ['string']],
    'HashMapDelete': ['void', ['uint64']],
    'HashMapGet': ['uint64', ['uint64', 'uint64', 'string', stringPtr]],
    'HashMapGetInt': ['uint64', ['uint64', 'uint64', stringPtr]],
    'HashMapGetAll': ['uint64', ['uint64']],
    'HashMapIteratorDelete': ['void', ['uint64']],
    'HashMapIteratorNext': ['int', ['uint64', ui64Ptr, stringPtr, ui64Ptr, stringPtr]],
    'HashMapIteratorNextInt': ['int', ['uint64', ui64Ptr, ui64Ptr, stringPtr]],
});
