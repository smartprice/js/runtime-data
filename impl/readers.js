function JSONReader() {
    this.read = function(buf) {
        return JSON.parse(buf.toString());
    };
};

function UI64Reader() {
    this.read = function(buf) {
        return Number(buf.readBigUInt64BE(0));
    };
};

module.exports = {
    'JSON': new JSONReader(),
    'UI64': new UI64Reader()
};
